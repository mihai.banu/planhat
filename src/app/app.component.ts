import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { interval } from 'rxjs';

export const CUST_NUMBER = 2000;
export const STANDARD_COST = 500;
export const ENTERPRISE_COST = 1000;
export const SECTOR = ['PUBLIC', 'PRIVATE'];
export const LOCATION = ['EUROPE', 'NORTH AMERICA', 'ASIA'];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  public custNumber = CUST_NUMBER;
  public customers = [];
  public totalUsers;
  public maxScore = 0; //used to calculate the rating based on the highest possible health

  public subscription; //used for simulation
  public counter; // sets counter in child
  public lostCustomers = [];
  public wonCustomers = [];

  public churn = 10; // 10% default value
  public newBiz  = 15;  // 15% default value

  public constructor(private changeDetector: ChangeDetectorRef) {}

  public ngOnInit() {
    // [REFACTOR] Move this to function
    for (let i = 0; i < this.custNumber; i++) {

      const totalUsers = this.getAllUsers(i);
      const customerSince = this.getStartDate();
      const channels = this.getChannels(0.1 * totalUsers); // assuming for each 10 users there is maximum a channel - randomly generated as well; min 3 channels
      const activeUsers = this.getActiveUsers(0.5 * totalUsers, 0.8 * totalUsers) // assuming between 50% - 80% of the users are active
      const messagesPerDay = this.getMessages(activeUsers); // assuming between 7-700 messages per active user

      const monthlyValue = this.determinePlatform(totalUsers);
      const lastRenewalDate = this.getLastRenewal();
      const nextRenewalDate = this.getNextRenewal();

      const healthScore = this.determineHealthIndex(totalUsers, activeUsers, messagesPerDay, channels, monthlyValue);

      const sector = SECTOR[this.random(0,1)];
      const location = LOCATION[this.random(0,2)];

      this.customers.push({ totalUsers, customerSince, channels, activeUsers, messagesPerDay, monthlyValue, lastRenewalDate, nextRenewalDate, healthScore, sector, location})
    }
    this.setHealthRatings();
  }

  public getAllUsers(currentUser) { // average about 30 users for 2000 customers
    if (currentUser <= (this.custNumber * 0.03)) { // assuming 3% are between 200-500 users
      return this.random(200, 500);
    }
    if (currentUser >= (this.custNumber * 0.03) && currentUser < (this.custNumber * 0.083)) { // assuming 5.3% are between 30-200 users
      return this.random(30, 200);
    }
    if (currentUser >= (this.custNumber * 0.083) && currentUser < (this.custNumber)) { // assuming 91.7% are between 0-30 users
      return this.random(1, 30);
    }
  }

  public getStartDate() {
    return new Date(new Date().setFullYear(new Date().getFullYear() - 5));  // 5 years ago
  }

  public getLastRenewal() {
    return new Date(new Date().getFullYear(), new Date().getMonth(), 1); // assuming each renewal happens on the 1st of the month
  }

  public getNextRenewal() {
    return new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1);
  }

  public getChannels(limit) {
    return this.random(3, limit) > 3 ? this.random(3, limit) : 3;
  }

  public getActiveUsers(minUsers, maxUsers) {
    return this.random(minUsers, maxUsers);
  }

  public getMessages(userNo) {
    if (userNo) {
      const userMessages = Array.from(Array(userNo).keys()); // array of user Messages
      const reducer = (acc) => acc + this.random(7, 700); // assuming between 7 and 700 messages per user per day
      return userMessages.reduce(reducer);
    } else {
      return 0;
    }
  }

  public determinePlatform(totalUsers) {
    if ((STANDARD_COST + 10 * totalUsers ) < (ENTERPRISE_COST + totalUsers)) {
      return STANDARD_COST + 10 * totalUsers; // standard platform
    } else {
      return ENTERPRISE_COST + totalUsers; // enterprise platform
    }
  }

  public determineHealthIndex(total, active, messages, channels, cost) {
    const costPerUser = cost/total;
    const valuePerUser = messages ? messages/active : 1 / active; //setting min 1 message in case no messages

    if (this.maxScore < Math.floor(valuePerUser/costPerUser)){
      this.maxScore = Math.floor(valuePerUser/costPerUser);
    }

    return Math.floor(valuePerUser/costPerUser);
  }

  public setHealthRatings() {
    this.customers.forEach((elem) => {
      switch (true) {
        case (elem.healthScore <= this.maxScore / 10) :
          elem.healthScore = 1;
          break;
        case (elem.healthScore > this.maxScore/10 && elem.healthScore <= 2*this.maxScore/10) :
          elem.healthScore = 2;break;
        case (elem.healthScore > 2*this.maxScore/10 && elem.healthScore <= 3* this.maxScore/10) :
          elem.healthScore = 3;break;
        case (elem.healthScore > 3*this.maxScore/10 && elem.healthScore <= 4* this.maxScore/10) :
          elem.healthScore = 4;break;
        case (elem.healthScore > 4*this.maxScore/10 && elem.healthScore <= 5* this.maxScore/10) :
          elem.healthScore = 5;break;
        case (elem.healthScore > 5*this.maxScore/10 && elem.healthScore <= 6* this.maxScore/10) :
          elem.healthScore = 6;break;
        case (elem.healthScore > 6*this.maxScore/10 && elem.healthScore <= 7* this.maxScore/10) :
          elem.healthScore = 7;break;
        case (elem.healthScore > 7*this.maxScore/10 && elem.healthScore <= 8* this.maxScore/10) :
          elem.healthScore = 8;break;
        case (elem.healthScore > 8*this.maxScore/10 && elem.healthScore <= 9* this.maxScore/10) :
          elem.healthScore = 9;break;
        case (elem.healthScore > 9*this.maxScore/10) :
          elem.healthScore = 10;break;
      }

    })

    // sorting desc by health score so that it is easier for when removing customers
    this.customers.sort((a,b) => (a.healthScore > b.healthScore) ? 1 : ((b.healthScore > a.healthScore) ? -1 : 0));
  }

  public random(min, max) { // int
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  public randomDec(min, max) { // decimals
    return Math.random() * (max - min + 1) + min;
  }

  // DELIVERY C - SIMULATION

  public triggerSimulation() {
    const source = interval(1000);

    this.subscription = source.subscribe( val => {
      console.log('Days passed ----- ', val* 6);
      this.counter = val;

      const customerChange = Math.floor(this.custNumber/this.randomDec(3, 5)); // assuming there are at least 20-33% changes every 6 days

      // Generating new random vals
      // [REFACTOR] Move this to function
      for (let i = 0; i < customerChange; i++) {
        const totalUsers = this.getAllUsers(i);
        const customerSince = this.getStartDate();
        const channels = this.getChannels(0.1 * totalUsers); // assuming for each 10 users there is maximum a channel - randomly generated as well; min 3 channels
        const activeUsers = this.getActiveUsers(0.5 * totalUsers, 0.8 * totalUsers) // assuming between 50% - 80% of the users are active
        const messagesPerDay = this.getMessages(activeUsers); // assuming between 7-700 messages per active user

        const monthlyValue = this.determinePlatform(totalUsers);
        const lastRenewalDate = this.getLastRenewal();
        const nextRenewalDate = this.getNextRenewal();

        const healthScore = this.determineHealthIndex(totalUsers, activeUsers, messagesPerDay, channels, monthlyValue);

        const randCustPos = this.random(0, this.custNumber-1);

        this.customers[randCustPos] = {
          totalUsers, customerSince, channels, activeUsers, messagesPerDay, monthlyValue, lastRenewalDate, nextRenewalDate, healthScore,
          section : this.customers[randCustPos].section,
          location : this.customers[randCustPos]. location
        }

      }

      console.log('total cust', this.customers.length)
      // Removing customers as per churn
      const customersLength = this.customers.length;
      const lengthMinusChurn = Math.floor((100 - this.churn)*customersLength/100);
      console.log('to delete:', customersLength - lengthMinusChurn);

      for (let i = customersLength - 1; i >= lengthMinusChurn; i--){
        const randCustPos = Math.floor(Math.random()*this.customers.length);

        this.lostCustomers.push(this.customers[randCustPos]);
        this.customers.splice(randCustPos, 1);
      }

      this.custNumber = this.customers.length;

      // Adding customers as per new biz
      const newBizCustNo = Math.floor(this.newBiz*this.customers.length/100);
      console.log('to gain', newBizCustNo);

      // [REFACTOR] Move this to function
      for (let i = 0; i < newBizCustNo; i++) {

        const totalUsers = this.getAllUsers(i);
        const customerSince = this.getStartDate();
        const channels = this.getChannels(0.1 * totalUsers); // assuming for each 10 users there is maximum a channel - randomly generated as well; min 3 channels
        const activeUsers = this.getActiveUsers(0.5 * totalUsers, 0.8 * totalUsers) // assuming between 50% - 80% of the users are active
        const messagesPerDay = this.getMessages(activeUsers); // assuming between 7-700 messages per active user

        const monthlyValue = this.determinePlatform(totalUsers);
        const lastRenewalDate = this.getLastRenewal();
        const nextRenewalDate = this.getNextRenewal();

        const healthScore = this.determineHealthIndex(totalUsers, activeUsers, messagesPerDay, channels, monthlyValue);

        const sector = SECTOR[this.random(0,1)];
        const location = LOCATION[this.random(0,2)];

        this.wonCustomers.push({ totalUsers, customerSince, channels, activeUsers, messagesPerDay, monthlyValue, lastRenewalDate, nextRenewalDate, healthScore, sector, location})
      }

      this.customers = this.customers.concat(this.wonCustomers);

      // close if too many users
      if (this.customers.length > 15000) {
        alert('Limit reached. Unable to render charts!');
        this.stopSimulation();
      }

      // sorting desc by health score so that it is easier for when removing customers
      this.customers.sort((a,b) => (a.healthScore > b.healthScore) ? 1 : ((b.healthScore > a.healthScore) ? -1 : 0));
    });
  }

  public stopSimulation() {
    this.subscription.unsubscribe();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
