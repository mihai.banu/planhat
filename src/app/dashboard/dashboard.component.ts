import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import * as c3 from 'c3';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Input() customers;
  @Input() wonCustomers = [];
  @Input() lostCustomers = [];
  @Input() counter;

  public limitedCustomers;

  public activeTab = 0

  constructor(private changeDetector: ChangeDetectorRef) { }

  public generateLineChart() {

    const activeUsers = this.customers.map(elem => elem.activeUsers);
    const totalUsers = this.customers.map(elem => elem.totalUsers);

    let chart = c3.generate({
      bindto: '#chart',
      data: {
        columns: [
          ['Active Users', ...activeUsers],
          ['Total Users', ...totalUsers]
        ]
      }
    });
  }

  public generateGauge() {

    const lessThan30 = this.customers.filter(elem => elem.totalUsers < 30);

    let gauge = c3.generate({
      bindto: '#gauge',
      data: {
          columns: [
              ['Customers with less than 30 users', (lessThan30.length/this.customers.length)*100],
          ],
          type: 'gauge',
      },
      gauge: {},
      color: {
          pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'],
          threshold: {
              values: [30, 60, 90, 100]
          }
      },
      size: {
          height: 300
      }
    });
  }

  public generateDonut() {

    const healthScorings = this.groupBy(this.customers, 'healthScore');

    let donut = c3.generate({
      bindto: '#donut',
      data: {
          columns: [
              ['1', healthScorings[1] ? healthScorings[1]: 0 ],
              ['2', healthScorings[2] ? healthScorings[2]: 0 ],
              ['3', healthScorings[3] ? healthScorings[3]: 0 ],
              ['4', healthScorings[4] ? healthScorings[4]: 0 ],
              ['5', healthScorings[5] ? healthScorings[5]: 0 ],
              ['6', healthScorings[6] ? healthScorings[6]: 0 ],
              ['7', healthScorings[7] ? healthScorings[7]: 0 ],
              ['8', healthScorings[8] ? healthScorings[8]: 0 ],
              ['9', healthScorings[9] ? healthScorings[9]: 0 ],
              ['10', healthScorings[10] ? healthScorings[10]: 0 ]
          ],
          type : 'donut',
      },
      donut: {
          title: "Customers By Health Score"
      }
    });

  }

  public groupBy(arr, property) {
    return arr.reduce(function (acc, object) {
      const key = object[property];
      if (!acc[key]) {
        acc[key] = 0;
      }
      ++acc[key];
    return acc;
    }, {});
  }

  ngOnInit() {

  }

  ngOnChanges(): void {
    if (this.customers.length) {
      this.limitedCustomers = this.customers.slice(1990);

      this.generateLineChart();
      this.generateGauge();
      this.generateDonut();
    }

  }

  public setActive(val) {
    this.activeTab = val;
    this.changeDetector.detectChanges();
  }

}
